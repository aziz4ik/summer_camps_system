window.onload = function() {
    //Check File API support
    if (window.File && window.FileList && window.FileReader) {
        var filesInput = document.getElementById("files");
        filesInput.addEventListener("change", function(event) {
            var files = event.target.files; //FileList object
            var output = document.getElementById("result");
            output.style.cssText = 'margin-left:30px; margin-top: 30px;'
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                //Only pics
                if (!file.type.match('image'))
                    continue;
                var picReader = new FileReader();
                picReader.addEventListener("load", function(event) {
                    var picFile = event.target;
                    var div = document.createElement("div");
                    div.style.cssText = 'float:left'
                    div.innerHTML = "<div><img class='thumbnail' src='" + picFile.result + "'" +
                        "title='" + picFile.name + "'/></div><div style='text-align: center'><i style='padding: 10px 10px; cursor:pointer;' class=\"fa fa-trash-alt delete-image-icon\"></i>\n</div></div>";
                    output.insertBefore(div, null);
                    // div.innerHTML = <input type="number" value={picFile.id} >
                });
                //Read the image
                picReader.readAsDataURL(file);
            }
        });
    } else {
        console.log("Your browser does not support File API");
    }
}


