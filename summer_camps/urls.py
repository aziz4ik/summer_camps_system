from django.contrib import admin
from django.urls import path, include
from summer_camps import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns

urlpatterns = [
    path('django_admin/', admin.site.urls),
]
urlpatterns += i18n_patterns(
    path('', include('apps.camps.urls')),
    path('', include('apps.telegram_auth.urls')),
    path('', include('apps.admin_dashboard.urls')),
    path('', include('apps.user.urls')),
    path('', include('apps.reservation.urls')),
    path('', include('apps.one_id_auth.urls')),
    path('', include('apps.stats.urls')),
)
urlpatterns += static(settings.STATIC_URL)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
