from django.shortcuts import render
from django.views.generic import TemplateView


class StatsTestView(TemplateView):
    template_name = 'admin_interface/stats.html'
