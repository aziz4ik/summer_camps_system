from django.urls import path

from apps.stats.views import StatsTestView

urlpatterns = [
    path('stats/', StatsTestView.as_view(), name='stats')
]