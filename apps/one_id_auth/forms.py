from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import forms


class SignUpForm(UserCreationForm):
    phone_number = forms.CharField(max_length=12, required=True)

    class Meta:
        model = User
        fields = ('username', 'phone_number', 'password1', 'password2',)
