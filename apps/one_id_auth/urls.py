from django.urls import path
from apps.one_id_auth.views import eauth, eauthcallback, Login

app_name = 'one_id_auth'

urlpatterns = [
    path('login/', Login.as_view(), name='login'),
    path('eauth/', eauth, name='eauth'),
    path('eauthcallback/', eauthcallback, name='eauthcallback'),
]