from django.apps import AppConfig


class OneIdAuthConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.one_id_auth'
