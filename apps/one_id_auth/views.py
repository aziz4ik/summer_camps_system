import requests

from multiprocessing.connection import Client
from random import randint

from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.shortcuts import render, redirect

# Create your views here.
# from one_id_auth.forms import SignUpForm
from django.views.generic import TemplateView


class Login(TemplateView):
    template_name = 'other/login.html'


def eauth(request):
    host = request.headers.get('host')
    client_id = 'test.epmty.uz'
    redirect_uri = 'http://' + host + '/eauthcallback/'
    id_egov_url = 'http://sso.egov.uz:8443/sso/oauth/Authorization.do' + '?client_id=' + client_id
    url = id_egov_url + '&response_type=one_code&redirect_uri=' + redirect_uri + '&state=12&scope=legal'
    return redirect(url)


def eauthcallback(request):
    User = get_user_model()
    code = request.GET.get('code')
    post = {
        'grant_type': 'one_authorization_code',
        'client_id': 'test.epmty.uz',
        'client_secret': 'As9d1Gu6fa/0q5yw+Zwfgw==',
        'redirect_uri': '',
        'code': code,
    }

    response = requests.post('http://sso.egov.uz:8443/sso/oauth/Authorization.do', data=post)

    data = response.json()

    access_post = {
        'grant_type': 'one_access_token_identify',
        'client_id': 'test.epmty.uz',
        'client_secret': 'As9d1Gu6fa/0q5yw+Zwfgw==',
        'access_token': data['access_token'],
        'scope': 'legal',
    }

    access_response = requests.post('http://sso.egov.uz:8443/sso/oauth/Authorization.do', data=access_post)

    access_data = access_response.json()

    password = access_data['pport_no']
    email = access_data['email']
    first_name = access_data['first_name']
    last_name = access_data['sur_name']
    username = access_data['user_id']

    pin = access_data['pin']
    tin = access_data['tin']
    pport_no = access_data['pport_no']
    mid_name = access_data['mid_name']
    full_name = access_data['full_name']
    user_type = access_data['user_type']
    mob_phone_no = access_data['mob_phone_no']
    birth_date = access_data['birth_date']
    birth_place = access_data['birth_place']
    user_type = access_data['user_type']

    try:
        birth_cntry = access_data['ctzn']
    except Exception:
        birth_cntry = None

    try:
        per_adr = access_data['per_adr']
    except Exception:
        per_adr = None

    try:
        obj = User.objects.get(username=username)
    except User.DoesNotExist:
        obj = User.objects.create_user(
            username=username,
            password=password,
            email=email,
            first_name=first_name,
            last_name=last_name,
            mid_name=mid_name,
            address=per_adr,
            phone_number=mob_phone_no,
            pport_no=pport_no,
        )
        obj.save()

    user = authenticate(username=username, password=password)

    if user is not None:
        login(request, user)

    return redirect('/')


random_five_digit_otp = randint(10000, 99999)


def confirm_number(phone_number_to_send_otp):
    account_sid = 'ACa0f88233a43b6846fba410ac5cf1e6dc'
    auth_token = '48889650aa287fedcadcd2f5a053e650'
    client = Client(account_sid, auth_token)

    message = client.messages.create(
        from_='+13203810347',
        body='Sizning ENTV platformasidan ro\'yxatdan o\'tish uchun bir martalik raqam {0}'
            .format(random_five_digit_otp),
        to=phone_number_to_send_otp
    )

    print(message.sid)


# class RegisterUser(TemplateView):
#     template_name = 'signup.html'


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            # Send http request to Twilio
            # Redirect to the verify phone number page
            # Verify phone number
            # Continue below process
            form.save()
            phone_number = form.cleaned_data.get('phone_number')
            confirm_number(phone_number)
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = UserCreationForm()
    return render(request, 'templates/original_templates/user_interface/signup.html', {'form': form})
