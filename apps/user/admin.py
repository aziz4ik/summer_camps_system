from django.contrib import admin
from django.contrib.auth.models import Permission

from apps.user.models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'get_group', )
    filter_horizontal = ('groups', 'user_permissions')
    # list_filter = ('groups',)

    def get_group(self, obj):
        """
        get group, separate by comma, and display empty string if user has no group
        """
        return ','.join([g.name for g in obj.groups.all()]) if obj.groups.count() else ''


admin.site.register(Permission)
