from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import CASCADE

from apps.locations.models import City
from apps.reservation.models import TicketReservation
from apps.core.constants.user_child_gender import CHILD_GENDER
from apps.core.models.base_model import BaseModel
from apps.camps.models import Camp


class User(AbstractUser):
    city = models.ForeignKey(City, CASCADE, null=True, blank=True)
    camp = models.ForeignKey(Camp, CASCADE, null=True, blank=True)
    mid_name = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    phone_number = models.CharField(max_length=255, null=True, blank=True)
    pport_no = models.CharField(max_length=255, null=True, blank=True)


class Child(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    gender = models.CharField(max_length=255, null=True, blank=True)
    age = models.PositiveIntegerField()
    shift = models.ForeignKey('camps.Shift', on_delete=models.CASCADE, null=True, blank=True)
    reservation = models.ForeignKey(TicketReservation, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
