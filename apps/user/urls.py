from django.urls import path

from apps.user import views
from apps.user.views import logout_user

urlpatterns = [
    path('users/', views.IndexView.as_view(), name='index'),
    path('users/<int:pk>/', views.UserDetailView.as_view(), name='detail'),
    path('users/edit/<int:pk>/', views.edit, name='edit'),
    path('users/create/', views.create, name='create'),
    path('users/delete/<int:pk>/', views.delete, name='delete'),

    path('logout/', logout_user, name='logout')
]
