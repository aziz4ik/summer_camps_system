from django import forms
from .models import User, Child
from django.forms import ModelForm
from django.forms import formset_factory


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = "__all__"


class ChildFrom(ModelForm):
    first_name = forms.CharField(
        label='Ismi',
        required=False,
        disabled=False,
    )
    last_name = forms.CharField(
        label='Familiyasi',
        required=False,
        disabled=False,
    )
    gender = forms.CharField(
        label='Jinsi',
        required=False,
        disabled=False,
    )
    birth_date = forms.CharField(
        label='Yoshi',
        required=False,
        disabled=False,
    )

    class Meta:
        model = Child
        fields = [
            'first_name',
            'last_name',
            'gender',
            'birth_date',
        ]


ChildFormset = formset_factory(ChildFrom, extra=2)
