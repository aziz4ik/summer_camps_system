from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView, ListView, DetailView
from apps.user.forms import UserForm
from apps.user.models import User


class IndexView(ListView):
    model = User
    queryset = User.objects.all()
    template_name = 'admin_interface/users/index.html'
    context_object_name = 'user_list'


class UserDetailView(DetailView):
    model = User
    template_name = 'admin_interface/users/show.html'


def create(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    form = UserForm()

    return render(request, 'admin_interface/users/create.html', {'form': form})


def edit(request, pk, template_name='admin_interface/users/edit.html'):
    user = get_object_or_404(User, pk=pk)
    form = UserForm(request.POST or None, instance=user)
    if form.is_valid():
        form.save()
        return redirect('index')
    return render(request, template_name, {'form': form})


def delete(request, pk):
    contact = get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        contact.delete()
        return redirect('index')


def logout_user(request):
    logout(request)
    return redirect('/')
