from django.urls import path

from apps.telegram_auth.views import TestTemplateView

urlpatterns = [
    path('auth', TestTemplateView.as_view(), name='auth')
]