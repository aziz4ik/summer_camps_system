from django.shortcuts import render
from django.views.generic import TemplateView


class TestTemplateView(TemplateView):
    template_name = 'other/test_page.html'
