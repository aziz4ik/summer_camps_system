from django.contrib.auth.models import User
from django.db import models
from django.db.models import CASCADE

from apps.core.models.base_model import BaseModel
from apps.camps.models import Camp


class PhoneNumber(BaseModel):
    camp = models.ForeignKey(Camp, CASCADE)

    number = models.CharField(max_length=255)

    def __str__(self):
        return self.number


class News(BaseModel):
    headline = models.CharField(max_length=255)
    description = models.TextField()
    main_image = models.FileField()
