

SUMMER = 'summer'
WINTER = 'winter'
FALL = 'fall'
SPRING = 'spring'

CAMP_SEASONS = (
    (SUMMER, 'summer'),
    (WINTER, 'winter'),
    (FALL, 'fall'),
    (SPRING, 'spring'),
)
