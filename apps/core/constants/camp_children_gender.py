

BOYS = 'boys'
GIRLS = 'girls'
MIXED = 'mixed'

SQUAD_CHILDREN_GENDER = (
    (BOYS, 'boys'),
    (GIRLS, 'girls'),
    (MIXED, 'mixed'),
)
