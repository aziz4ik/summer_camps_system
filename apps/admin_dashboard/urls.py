from django.urls import path

from apps.admin_dashboard.views.add_placement import PlacementCreateView
from apps.admin_dashboard.views.placement_list import PlacementListView
from apps.admin_dashboard.views.delete_camp_image import CampImageDeleteView
from apps.admin_dashboard.views.add_user_to_group import add_user_to_group
from apps.admin_dashboard.views.update_camp import CampUpdateView
from apps.admin_dashboard.views.update_squad import SquadUpdateView
from apps.admin_dashboard.views.update_advantage import AdvantageUpdateView
from apps.admin_dashboard.views.delete_advantage import AdvantageDeleteView
from apps.admin_dashboard.views.create_advantage import AdvantageCreateView
from apps.admin_dashboard.views.advantages_list import AdvantagesListView
from apps.admin_dashboard.views.dashboard import DashboardTemplateView
from apps.admin_dashboard.views.camp_detail import CampDetail
from apps.admin_dashboard.views.camps_list import CampsListView
from apps.admin_dashboard.views.create_camp import CampCreateView
from apps.admin_dashboard.views.create_shift import ShiftCreateView
from apps.admin_dashboard.views.create_squad import SquadCreateView
from apps.admin_dashboard.views.shifts_list import ShiftsListView
from apps.admin_dashboard.views.squad_list import SquadListView
from apps.admin_dashboard.views.update_shift import ShiftUpdateView


urlpatterns = [
    path('dashboard', DashboardTemplateView.as_view(), name='dashboard'),

    # Camp
    path('add_camp/', CampCreateView.as_view(), name='add_camp'),
    path('camps/list/', CampsListView.as_view(), name='camps_list'),
    path('camps/detail/', CampDetail.as_view(), name='camp_detail'),
    path('update_camp/<int:pk>/', CampUpdateView.as_view(), name='camp_update'),

    # Squad
    path('add_squad/', SquadCreateView.as_view(), name='add_squad'),
    path('squads/list/', SquadListView.as_view(), name='squads_list'),
    path('update_squad/<int:pk>/', SquadUpdateView.as_view(), name='update_squad'),

    # Shift
    path('add_shift/', ShiftCreateView.as_view(), name='add_shift'),
    path('shifts/list/', ShiftsListView.as_view(), name='shifts_list'),
    path('update_shift/<int:pk>/', ShiftUpdateView.as_view(), name='update_shift'),

    # Camp Image
    path('delete_camp_image/<int:pk>', CampImageDeleteView.as_view(), name='delete_camp_image'),

    # Settings
    path('add_advantage/', AdvantageCreateView.as_view(), name='add_advantage'),
    path('delete_advantage/<int:pk>/', AdvantageDeleteView.as_view(), name='delete_advantage'),
    path('update_advantage/<int:pk>/', AdvantageUpdateView.as_view(), name='update_advantage'),
    path('settings/advantages/', AdvantagesListView.as_view(), name='advantages_list'),

    # Roles
    path('add_user_to_group/', add_user_to_group, name='add_user_to_group'),

    # Placement
    path('placements/', PlacementListView.as_view(), name='placement_list'),
    path('add_placement/', PlacementCreateView.as_view(), name='add_placement'),
]
