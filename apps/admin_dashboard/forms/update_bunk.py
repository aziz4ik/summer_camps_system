from django import forms

from apps.camps.models import Bunk


class UpdateBunkModelForm(forms.ModelForm):
    class Meta:
        model = Bunk
        fields = '__all__'
