from django import forms

from apps.camps.models import Placement


class CreatePlacementForm(forms.ModelForm):
    class Meta:
        model = Placement
        fields = '__all__'
