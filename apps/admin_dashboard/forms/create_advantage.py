from django import forms

from apps.camps.models import Advantage


class CreateAdvantageModelForm(forms.ModelForm):
    class Meta:
        model = Advantage
        fields = '__all__'
