from django import forms

from apps.camps.models import Squad


class CreateSquadModelForm(forms.ModelForm):
    class Meta:
        model = Squad
        fields = '__all__'
