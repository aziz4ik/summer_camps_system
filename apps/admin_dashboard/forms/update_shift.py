from django import forms

from apps.camps.models import Shift


class UpdateShiftModelForm(forms.ModelForm):
    class Meta:
        model = Shift
        fields = '__all__'
        exclude = ('camp',)
