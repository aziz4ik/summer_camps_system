from django import forms

from apps.camps.models import Shift


class CreateShiftModelForm(forms.ModelForm):
    class Meta:
        model = Shift
        fields = '__all__'
