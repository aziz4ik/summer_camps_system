from django import forms

from apps.camps.models import Camp


class UpdateCampForm(forms.ModelForm):
    class Meta:
        model = Camp
        fields = '__all__'
        exclude = ('rating',
                   'total_capacity',
                   'rating')
