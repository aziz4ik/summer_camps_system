from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView

from apps.camps.models import Advantage


class AdvantagesListView(LoginRequiredMixin, ListView):
    login_url = '/login'
    model = Advantage
    queryset = Advantage.objects.all()
    template_name = 'admin_interface/advantages.html'
    context_object_name = 'all_advantages'
