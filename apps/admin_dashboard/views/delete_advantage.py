from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DeleteView

from apps.camps.models import Advantage


class AdvantageDeleteView(LoginRequiredMixin, DeleteView):
    login_url = '/login'
    model = Advantage
    success_url = '/settings/advantages'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)
