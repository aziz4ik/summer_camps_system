from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DeleteView

from apps.camps.models import CampImage


class CampImageDeleteView(LoginRequiredMixin, DeleteView):
    login_url = '/login'
    model = CampImage
    success_url = '/camps/list/'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)
