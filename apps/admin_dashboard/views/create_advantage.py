from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView

from apps.admin_dashboard.forms.create_advantage import CreateAdvantageModelForm
from apps.camps.models import Advantage


class AdvantageCreateView(LoginRequiredMixin, CreateView):
    login_url = '/login'
    model = Advantage
    template_name = 'admin_interface/add_advantage.html'
    success_url = '/settings/advantages/'
    form_class = CreateAdvantageModelForm
