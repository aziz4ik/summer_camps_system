from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class CampDetail(LoginRequiredMixin, TemplateView):
    login_url = '/login'
    template_name = 'admin_interface/camp_detail.html'
