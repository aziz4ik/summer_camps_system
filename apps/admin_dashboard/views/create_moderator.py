from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class CreateModeratorCreateView(LoginRequiredMixin, TemplateView):
    login_url = '/login'
    template_name = 'admin_interface/add_moderator.html'
