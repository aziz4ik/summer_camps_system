from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

from apps.user.models import User
from django.contrib.auth.models import Group


@login_required
def add_user_to_group(request):
    if request.method == 'POST':
        role_type = request.POST.get('role_type')
        print(role_type)
        user_id = request.POST.get('user_id')
        print(user_id)

        get_user_from_db = User.objects.get(id=user_id)
        regional_group = Group.objects.get(name='regional')
        republican_group = Group.objects.get(name='republican')
        if role_type == 'regional':
            city = request.POST.get('city')
            print(city)
            regional_group.user_set.add(get_user_from_db)
            get_user_from_db.city_id = city
            get_user_from_db.save()
        elif role_type == 'republican':
            republican_group.user_set.add(get_user_from_db)

    return redirect('/users')
