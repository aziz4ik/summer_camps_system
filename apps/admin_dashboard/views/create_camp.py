from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.views.generic import CreateView

from apps.admin_dashboard.forms.create_camp import CreateCampForm
from apps.camps.models import Camp, CampImage, Advantage
from apps.locations.models import Region
from apps.main.models import PhoneNumber
from apps.user.models import User


class CampCreateView(LoginRequiredMixin, CreateView):
    login_url = '/login'
    model = Camp
    template_name = 'admin_interface/add_camp.html'
    success_url = '/camps/list/'
    form_class = CreateCampForm

    def form_valid(self, form):
        get_images = self.request.FILES.getlist('images[]')
        get_phone_numbers = self.request.POST.getlist('phone_number')
        get_advantages = self.request.POST.getlist('advantages')

        camp = form.save(commit=False)
        camp.save()
        for image in get_images:
            CampImage.objects.create(
                camp_id=camp.id,
                image=image
            )
        for phone_number in get_phone_numbers:
            if phone_number == '':
                continue
            PhoneNumber.objects.create(
                camp_id=camp.pk,
                number=phone_number
            )

        c = Camp.objects.get(id=camp.pk)
        for advantage in get_advantages:
            c.advantages.add(advantage)

        return redirect('/camps/list/')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Get linked to the user regions list
        user_id_from_request = self.request.user.id
        get_user_from_db = User.objects.get(id=user_id_from_request)
        get_user_linked_city_id = get_user_from_db.city_id
        regions_list = Region.objects.filter(city_id=get_user_linked_city_id)

        advantages = Advantage.objects.all()
        context['regions'] = regions_list
        context['advantages'] = advantages
        return context
