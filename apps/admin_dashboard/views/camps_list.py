from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count, Sum, OuterRef, Subquery, IntegerField
from django.views.generic import ListView

from apps.camps.models import Camp
from apps.user.models import User


class CampsListView(LoginRequiredMixin, ListView):
    login_url = '/login'
    model = Camp
    queryset = Camp.objects.all()
    template_name = 'admin_interface/camps_list.html'
    context_object_name = 'all_camps'

    def get_queryset(self):
        """
        Filter camps list, show to the user
        only camps within user's region
        :return: queryset
        """
        get_user_id = self.request.user.id
        get_region = User.objects.get(id=get_user_id).city_id

        camps = Camp.objects.filter(region__city_id=get_region)
        sum_camp_capacity = camps.annotate(total_camp_capacity_new=Sum('squad__capacity')) \
            .filter(pk=OuterRef('pk'))

        count_shifts = camps.annotate(number_of_shifts_new=Count('shift')) \
            .filter(pk=OuterRef('pk'))

        count_squads = camps.annotate(number_of_squads_new=Count('squad')) \
            .filter(pk=OuterRef('pk'))

        camps = camps.annotate(total_camp_capacity=Subquery(sum_camp_capacity.values('total_camp_capacity_new'),
                                                            output_field=IntegerField()))

        camps = camps.annotate(total_shifts=Subquery(count_shifts.values('number_of_shifts_new'),
                                                     output_field=IntegerField()))

        camps = camps.annotate(total_squads=Subquery(count_squads.values('number_of_squads_new'),
                                                     output_field=IntegerField()))

        return camps
