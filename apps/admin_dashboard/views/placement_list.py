from django.views.generic import ListView

from apps.camps.models import Placement


class PlacementListView(ListView):
    template_name = 'admin_interface/placement.html'
    model = Placement
    queryset = Placement.objects.all()
    context_object_name = 'all_placements'
