from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, CreateView

from apps.admin_dashboard.forms.create_placement import CreatePlacementForm
from apps.camps.models import Placement, Squad, Shift


class PlacementCreateView(LoginRequiredMixin, CreateView):
    model = Placement
    template_name = 'admin_interface/add_placement.html'
    form_class = CreatePlacementForm
    success_url = '/placements'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        get_user_camp = self.request.user.camp.id
        context['own_camp_squads_list'] = Squad.objects.filter(camp_id=get_user_camp)
        context['own_camp_shift_list'] = Shift.objects.filter(camp_id=get_user_camp)
        return context
