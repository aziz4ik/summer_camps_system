from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import UpdateView

from apps.admin_dashboard.forms.update_shift import UpdateShiftModelForm
from apps.camps.models import Shift


class ShiftUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/login'
    model = Shift
    template_name = 'admin_interface/update_shift.html'
    form_class = UpdateShiftModelForm

    def get_success_url(self):
        get_shift = Shift.objects.get(id=self.object.id)
        camp_id = get_shift.camp_id
        return '/shifts/list/?camp={}'.format(camp_id)
