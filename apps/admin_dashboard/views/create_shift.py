from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView

from apps.admin_dashboard.forms.create_shift import CreateShiftModelForm
from apps.camps.models import Shift, Camp


class ShiftCreateView(LoginRequiredMixin, CreateView):
    model = Shift
    template_name = 'admin_interface/add_shift.html'
    success_url = '/shifts/list'
    form_class = CreateShiftModelForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['camps_list'] = Camp.objects.all()
        return context

    def get_success_url(self):
        get_squad = Shift.objects.get(id=self.object.id)
        camp_id = get_squad.camp_id
        return '/shifts/list?camp={}'.format(camp_id)
