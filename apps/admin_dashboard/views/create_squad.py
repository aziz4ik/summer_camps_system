from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView

from apps.admin_dashboard.forms.create_squad import CreateSquadModelForm
from apps.camps.models import Squad, Camp


class SquadCreateView(LoginRequiredMixin, CreateView):
    login_url = '/login'
    model = Squad
    template_name = 'admin_interface/add_squad.html'
    success_url = '/squads/list/'
    form_class = CreateSquadModelForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['camps_list'] = Camp.objects.all()
        return context

    def get_success_url(self):
        get_squad = Squad.objects.get(id=self.object.id)
        camp_id = get_squad.camp_id
        return '/squads/list?camp={}'.format(camp_id)
