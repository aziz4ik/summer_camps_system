from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, UpdateView

from apps.admin_dashboard.forms.update_advantage import UpdateAdvantageModelForm
from apps.camps.models import Advantage


class AdvantageUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/login'
    model = Advantage
    template_name = 'admin_interface/update_advantage.html'
    success_url = '/settings/advantages/'
    form_class = UpdateAdvantageModelForm
