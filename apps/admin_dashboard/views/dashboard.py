from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class DashboardTemplateView(LoginRequiredMixin, TemplateView):
    login_url = '/login'
    template_name = 'admin_interface/dashboard.html'
