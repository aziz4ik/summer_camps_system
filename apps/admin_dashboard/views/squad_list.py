from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.views.generic import ListView

from apps.camps.models import Squad, Camp


class SquadListView(LoginRequiredMixin, ListView):
    login_url = '/login'
    model = Squad
    queryset = Squad.objects.all()
    context_object_name = 'all_squads'
    template_name = 'admin_interface/squads.html'

    def get_queryset(self):
        # TODO: Write validation here to list only camps
        #  which user owns
        camp_id = self.request.GET.get('camp')
        return self.queryset.filter(camp_id=camp_id)

    def get(self, request, *args, **kwargs):
        camp_id = self.request.GET.get('camp')
        if not camp_id:
            return HttpResponse('You must specify camp id in order to see squads list')
        else:
            return super().get(request)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        camp_id = self.request.GET.get('camp')
        context['camp_name'] = Camp.objects.get(id=camp_id)
        return context
