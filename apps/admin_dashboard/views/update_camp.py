from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.views.generic import CreateView, UpdateView

from apps.admin_dashboard.forms.update_camp import UpdateCampForm
from apps.camps.models import Camp, CampImage, Advantage
from apps.locations.models import Region
from apps.main.models import PhoneNumber
from apps.user.models import User


class CampUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/login'
    model = Camp
    template_name = 'admin_interface/update_camp.html'
    success_url = '/camps/list/'
    form_class = UpdateCampForm

    def form_valid(self, form):
        get_images = self.request.FILES.getlist('images')
        get_phone_numbers = self.request.POST.getlist('phone_number')

        camp = form.save(commit=False)
        camp.save()
        for image in get_images:
            CampImage.objects.create(
                camp_id=camp.id,
                image=image
            )
        PhoneNumber.objects.filter(camp_id=camp.id).delete()
        for phone_number in get_phone_numbers:
            PhoneNumber.objects.create(
                camp_id=camp.pk,
                number=phone_number
            )
        return redirect('/camps/list/')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Get linked to the user regions list
        user_id_from_request = self.request.user.id
        get_user_from_db = User.objects.get(id=user_id_from_request)
        get_user_linked_city_id = get_user_from_db.city_id
        regions_list = Region.objects.filter(city_id=get_user_linked_city_id)

        advantages = Advantage.objects.all()
        context['regions'] = regions_list
        context['advantages'] = advantages
        return context
