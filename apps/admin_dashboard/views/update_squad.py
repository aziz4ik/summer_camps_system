from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import UpdateView

from apps.admin_dashboard.forms.update_squad import UpdateSquadModelForm
from apps.camps.models import Squad


class SquadUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/login'
    model = Squad
    template_name = 'admin_interface/update_squad.html'
    form_class = UpdateSquadModelForm

    def get_success_url(self):
        get_squad = Squad.objects.get(id=self.object.id)
        camp_id = get_squad.camp_id
        return '/squads/list/?camp={}'.format(camp_id)
