from django.http import HttpResponseForbidden, HttpResponseRedirect

from apps.user.models import User


class GroupLoginRequiredMixin(object):
    """
    View mixin which verifies that the user has authenticated.
    If group is specified by the parent class, then user must be member of that group (as set in django admin)
    """

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            user = User.objects.get(username=request.user.username)
            #if parent class has group attribute
            if hasattr(self, 'group'):
                grps = user.groups.filter(name__in=self.group)
                if not grps:
                    return HttpResponseForbidden()
        else:
            return HttpResponseRedirect('/login')
        return super(GroupLoginRequiredMixin, self).dispatch(request, *args, **kwargs)
