from apps.camps.models import Camp
from apps.user.models import User


def own_camps_list(request):
    request_user_id = request.user.id
    if request_user_id:
        user_city_id = User.objects.get(id=request_user_id).city_id
        camps = Camp.objects.filter(region__city_id=user_city_id)
        return {'camps_list': camps}
    if request_user_id is None:
        return {}
