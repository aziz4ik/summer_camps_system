from apps.locations.models import City


def get_all_cities_list(request):
    cities = City.objects.all()
    return {'cities': cities}
