from django.contrib import admin

from apps.camps.models import Camp, Advantage, CampSeason, CampImage, SquadLeaders
from apps.camps.models import Shift, Squad

admin.site.register(Camp)
admin.site.register(Advantage)
admin.site.register(CampSeason)
admin.site.register(CampImage)
admin.site.register(SquadLeaders)


@admin.register(Shift)
class ShiftAdmin(admin.ModelAdmin):
    list_display = ('camp', 'number')


@admin.register(Squad)
class SquadAdmin(admin.ModelAdmin):
    list_display = ['camp','number', 'capacity', 'gender', 'get_age_range']
    list_filter = ['camp', 'number']
    # filter_vertical = ['shift']

    def get_age_range(self, obj):
        return '{0} - {1}'.format(obj.from_age, obj.until_age)
