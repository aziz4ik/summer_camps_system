from django.views.generic import TemplateView
from apps.camps.models import Camp, Shift
from apps.user.models import Child
from django.shortcuts import render, get_object_or_404, redirect
from apps.reservation.forms import ReservationForm, ReservationFormSet, ChildModelForm
from apps.reservation.models import TicketReservation


def index(request):
    camps = Camp.objects.all()
    context = {
        'camps': camps
    }

    return render(request, 'other/index.html', context)


def camp_list(request):
    camps = Camp.objects.all()
    context = {
        'camps': camps
    }

    return render(request, 'other/camps-list.html', context)


# def camp_detail(request, pk):
#     camp = get_object_or_404(Camp, pk=pk)
#     # shift = get_object_or_404(Shift, camp_id=pk)
#     shifts = Shift.objects.filter(camp_id=pk)
#     context = {
#         'camp': camp,
#         'shifts': shifts
#     }
#     return render(request, 'other/camps-detail.html', context)


def cabinet(request):
    return render(request, 'other/cabinet.html')


def camp_detail(request, pk):
    camp = get_object_or_404(Camp, pk=pk)
    user = request.user
    shifts = Shift.objects.filter(camp_id=camp)
    template_name = 'other/camps-detail.html'
    # template_name = 'other/reservation_test.html'
    if request.method == 'GET':
        resform = ReservationForm(request.GET or None)
        formset = ReservationFormSet(queryset=Child.objects.none())
    elif request.method == 'POST':
        resform = ReservationForm(request.POST)
        formset = ReservationFormSet(request.POST)
        get_shift_id = request.POST.get('shift')
        if resform.is_valid() and formset.is_valid():
            res = resform.save(commit=False)
            res.user = user
            res.camp = camp
            res.save()
            for form in formset:
                children = form.save(commit=False)
                children.reservation = res
                children.user = user
                children.shift_id = get_shift_id
                children.save()
            return redirect('/')
    return render(request, template_name, {
        'resform': resform,
        'formset': formset,
        'camp': camp,
        'shifts': shifts,
        'user': user,
    })
