from django.urls import path

from apps.camps.views import index, camp_list, camp_detail, cabinet

app_name = 'camps'
urlpatterns = [
    path('', index, name='index'),
    path('camps_list/', camp_list, name='camps_list'),
    path('camps_detail/<int:pk>/', camp_detail, name='camps_detail'),
    path('cabinet/', cabinet, name='cabinet'),
]
