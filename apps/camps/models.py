from django.db import models
from django.db.models import CASCADE, PROTECT

from apps.core.constants.seasons import CAMP_SEASONS
from apps.core.models.base_model import BaseModel
from apps.core.constants.camp_children_gender import SQUAD_CHILDREN_GENDER
# from apps.user.models import User
from apps.core.constants.squad_type import SQUAD_STRUCTURE_TYPE
from apps.core.constants.leader_type import LEADER_TYPES
from apps.core.constants.user_child_gender import CHILD_GENDER


class Advantage(BaseModel):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Advantage'
        verbose_name_plural = 'Advantages'


class Camp(BaseModel):
    region = models.ForeignKey('locations.Region', PROTECT)
    advantages = models.ManyToManyField(Advantage)

    name = models.CharField(max_length=256)
    main_description = models.TextField(null=True, blank=True)
    about_description = models.TextField(null=True, blank=True)
    nutrition_description = models.TextField(null=True, blank=True)
    program_description = models.TextField(null=True, blank=True)

    address = models.CharField(max_length=255)
    cost = models.PositiveIntegerField(null=True, blank=True)
    rating = models.DecimalField(max_digits=3, decimal_places=2, null=True, blank=True)
    total_capacity = models.PositiveIntegerField(default=0, null=True, blank=True)
    date_opened = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.name


class CampSeason(BaseModel):
    camp = models.ForeignKey(Camp, CASCADE)
    season = models.CharField(max_length=255, choices=CAMP_SEASONS)

    def __str__(self):
        return '{0} (Season: {1})'.format(self.camp, self.season)

    class Meta:
        unique_together = ('camp', 'season')


class Shift(BaseModel):
    camp = models.ForeignKey(Camp, CASCADE)

    number = models.PositiveIntegerField()
    capacity = models.PositiveIntegerField(null=True, blank=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()

    def __str__(self):
        return '{0} {1} shift'.format(self.camp.name, self.number)


class Squad(BaseModel):
    camp = models.ForeignKey(Camp, CASCADE, null=True, blank=True)

    gender = models.CharField(max_length=50, choices=SQUAD_CHILDREN_GENDER)
    squad_structure_type = models.CharField(max_length=50, choices=SQUAD_STRUCTURE_TYPE)
    number = models.PositiveIntegerField()
    from_age = models.PositiveIntegerField()
    until_age = models.PositiveIntegerField()
    capacity = models.PositiveIntegerField()

    def __str__(self):
        return '{0}'.format(self.number)

    class Meta:
        unique_together = ('camp', 'number')


class CampImage(BaseModel):
    camp = models.ForeignKey(Camp, CASCADE)
    image = models.FileField(upload_to='')
    is_primary = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.image}'


class Placement(BaseModel):
    squad = models.ForeignKey(Squad, CASCADE)
    shift = models.ForeignKey(Shift, CASCADE)

    child_first_name = models.CharField(max_length=255)
    child_last_name = models.CharField(max_length=255)
    gender = models.CharField(max_length=55, choices=CHILD_GENDER)
    certificate_of_birth = models.CharField(max_length=55)
    bunk = models.CharField(max_length=255)
    birth_year = models.DateField()

    def __str__(self):
        return self.certificate_of_birth


class SquadLeaders(BaseModel):
    leader = models.ForeignKey('user.User', CASCADE, null=True, blank=True)
    squad = models.ForeignKey(Squad, CASCADE)
    shift = models.ForeignKey(Shift, CASCADE)

    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    leader_type = models.CharField(max_length=55, choices=LEADER_TYPES)
    phone_number = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
