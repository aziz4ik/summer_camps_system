from django.db import models
from django.db.models import CASCADE

from apps.core.models.base_model import BaseModel
# from apps.main.models import Shift
from apps.camps.models import Shift
from apps.user.models import User


class TicketPurchase(BaseModel):
    # camp = models.ForeignKey(Camp, CASCADE)
    user = models.ForeignKey(User, CASCADE)
    shift = models.ForeignKey(Shift, CASCADE)
    # squad = models.F