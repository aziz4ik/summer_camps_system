from django.contrib import admin

# Register your models here.
from apps.locations.models import Region, City

admin.site.register(City)


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_filter = ('city',)
