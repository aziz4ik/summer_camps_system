from django.db import models

# Create your models here.
from django.db.models import CASCADE

from apps.core.models.base_model import BaseModel


class City(BaseModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'


class Region(BaseModel):
    city = models.ForeignKey(City, CASCADE)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
