from django.urls import path

from apps.reservation.views import ReservationsListView

urlpatterns = [
    path('reservations/', ReservationsListView.as_view(), name='reservations')
]
