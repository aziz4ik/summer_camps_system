# Generated by Django 3.2.3 on 2021-06-28 06:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('camps', '0002_initial'),
        ('reservation', '0002_ticketreservation_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticketreservation',
            name='camp',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='camps.camp'),
        ),
    ]
