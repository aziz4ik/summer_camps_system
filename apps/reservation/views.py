from django.shortcuts import render
from django.views.generic import TemplateView


class ReservationsListView(TemplateView):
    template_name = 'admin_interface/reservations.html'
