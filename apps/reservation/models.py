from django.contrib.auth.models import User
from django.db import models

from apps.core.models.base_model import BaseModel
from summer_camps import settings


class TicketReservation(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    camp = models.ForeignKey('camps.Camp', on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    expiration_date = models.CharField(max_length=255, null=True, blank=True)
    is_expired = models.CharField(max_length=255, null=True, blank=True)
