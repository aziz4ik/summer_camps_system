from django import forms
from django.forms import formset_factory, modelformset_factory

from apps.reservation.models import TicketReservation
from apps.user.models import Child
from apps.camps.models import Shift

MALE = 'M'
FEMALE = 'F'
GENDERS = [
    (MALE, 'O\'gil'),
    (FEMALE, 'Qiz'),
]


class ReservationForm(forms.ModelForm):
    class Meta:
        model = TicketReservation
        fields = ('name',)
        labels = {
            'name': 'Book Name'
        }
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Book Name here'
            }
            )
        }


class ChildModelForm(forms.ModelForm):
    name = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )

    gender = forms.ChoiceField(
        choices=GENDERS,
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    age = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )

    class Meta:
        model = Child
        fields = (
            'name',
            'gender',
            'age',
            'shift',
        )

    # def __init__(self, camp, *args, **kwargs):
    #     super(ChildModelForm, self).__init__(*args, **kwargs)
    #     self.fields['camp'].queryset = Shift.objects.filter(camp=camp)


# ReservationFormSet = modelformset_factory(
#     Child,
#     fields=('name', 'gender', 'age', 'shift'),
#     extra=1,
#     widgets={
#         'name': forms.TextInput(attrs={
#             'class': 'form-control',
#             'placeholder': 'FISH'
#         }),
#         'gender': forms.TextInput(attrs={
#             'class': 'form-control',
#             'placeholder': 'jinsi'
#         }),
#         'age': forms.TextInput(attrs={
#             'class': 'form-control',
#             'placeholder': 'Yoshi'
#         }),
#     }
# )

ReservationFormSet = modelformset_factory(
    Child,
    form=ChildModelForm,
)
